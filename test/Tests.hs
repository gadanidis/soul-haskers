import Test.Hspec
import Fusion
import DemonTypes
import DemonList (allDemons)

-- convenience functions for writing tests
demon :: Name -> Demon
demon query = head $ filter ((== query) . name) allDemons

testCalc :: String -> String -> String -> SpecWith ()
testCalc x y z = it (x ++ " + " ++ y ++ " = " ++ z) $
    binaryFuse (demon x) (demon y) `shouldBe` (Just $ demon z)

main :: IO ()
main = hspec $ do
    describe "Non-dark fusion calculations" $ do
        testCalc "Harpy" "Angel" "Jack Frost"
        testCalc "Succubus" "Peri" "Hanuman"
        testCalc "Makara" "Silky" "Genbu"
        testCalc "Makara" "Vivian" "Genbu"
        testCalc "Kuda" "Billiken" "Kamapua'a"
        testCalc "Oliver" "Billiken" "Kamapua'a"
        testCalc "Genbu" "Tuofei" "Thunderbird"
        testCalc "Qing Niuguai" "Tuofei" "Thunderbird"
        testCalc "Kukunochi" "Scathach" "Troll"
        testCalc "Tuofei" "Throne" "Troll"
        testCalc "Vodyanik" "Bes" "Pyro Jack"
        testCalc "Silky" "Genbu" "Vouivre"
    describe "Dark fusion calculations" $ do
        testCalc "Loki" "Cernunnos" "Mot"
        testCalc "Surt" "Saturnus" "Mot"
        testCalc "Mad Gasser" "Garrote" "Yaka"
