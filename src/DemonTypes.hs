module DemonTypes where

data Race = Deity
          | Enigma
          | Entity
          | Zealot
          | Megami
          | Fury
          | Lady
          | Kishin
          | Genma
          | Herald
          | Dragon
          | Avian
          | Avatar
          | Holy
          | Tree
          | Yoma
          | Fairy
          | Night
          | Divine
          | Fallen
          | Snake
          | Flight
          | Beast
          | UMA
          | Jirae
          | Brute
          | Femme
          | Vile
          | Reaper
          | Tyrant
          | Drake
          | Raptor
          | Jaki
          | Haunt
          | Spirit
          | Undead
          | Wilder
          | Wood
          | Foul
          | Element EType
          | Mitama MType
          deriving (Eq, Ord, Show, Read)

data EType = Gnome
           | Salamander
           | Sylph
           | Undine
           deriving (Eq, Ord, Show, Read)

data MType = Kusi
           | Ara
           | Saki
           | Nigi
           deriving (Eq, Ord, Show, Read)

data Demon = Demon { race :: Race
                   , name :: Name
                   , level :: Level
                   , darkness :: Darkness }
    deriving (Eq, Show)

data Darkness = Light
              | Normal
              | Dark
              deriving (Eq, Show)

type Level = Int
type Name = String

