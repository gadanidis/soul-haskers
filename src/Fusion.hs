module Fusion where

import DemonTypes
import FusionCharts (undarkChart, darkChart)
import DemonList (allDemons)

import Safe (headMay)
import Data.Maybe (fromJust)
import Data.List (intercalate)
import qualified Data.Map.Strict as M

chartLookup ::  M.Map Race (M.Map Race Race) -> (Race -> Race -> Bool) -> Race -> Race -> Maybe Race
-- Return the result race of fusing two races in the fusion chart, or Nothing if
-- the fusion is not possible.
chartLookup chart orientation race1 race2 =
    if race1 `orientation` race2
       then go race1 race2 chart
       else go race2 race1 chart
           where go r1 r2 c = M.lookup r2 (fromJust $ M.lookup r1 c)

undarkRace :: Race -> Race -> Maybe Race
undarkRace = chartLookup undarkChart (<=)

darkRace :: Race -> Race -> Maybe Race
darkRace = chartLookup darkChart (>=)

isDark :: Demon -> Bool
isDark d = case darkness d of
             Dark -> True
             _    -> False

binaryFuse :: Demon -> Demon -> Maybe Demon
-- Return the result of fusing two demons, or Nothing if the fusion is not possible.
binaryFuse d1 d2 = match newRace (level d1) (level d2)
    where newRace = if any isDark [d1, d2]
                      then darkRace (race d1) (race d2)
                      else undarkRace (race d1) (race d2)

match :: Maybe Race -> Level -> Level -> Maybe Demon
-- Find what demon would be fused for a given race given the levels of the source demons,
-- or Nothing if the race given is Nothing.
match Nothing _ _ = Nothing
match r x y = Just $ head $ filter (\a -> race a == newRace && level a == newLevel) allDemons
    where levels = map level (filter (\a -> race a == newRace) allDemons)
          newRace = fromJust r
          newLevel = closest avg levels
          avg = 2 + (x + y) `div` 2

closest :: Int -> [Int] -> Int
-- Find the nearest integer to x that's larger than x in a list of integers.
-- Take the largest integer in the list if none is larger than x.
closest x list = if all (x >) list
                    then maximum list
                    else minimum $ filter (> x) list

findDemon :: Name -> [Demon] -> Maybe Demon
-- Look up a demon by name in a list of demons, or Nothing if the demon does not exist.
findDemon query demons = headMay $ filter ((== query) . name) demons

showDemon :: Demon -> String
-- Convert a demon to a string.
showDemon d = intercalate ", " [ show (race d) ++ " " ++ name d
                               , "level " ++ show (level d)
                               , show (darkness d)
                               ]
