module DemonList (allDemons) where

import DemonTypes

allDemons :: [Demon]
allDemons =
    [ Demon Deity "Vishnu" 84  Normal
    , Demon Deity "Baal" 73  Normal
    , Demon Deity "Odin N" 66  Normal
    , Demon Deity "Odin" 65  Normal
    , Demon Deity "Osiris" 59  Normal
    , Demon Deity "Lugh" 51  Normal
    , Demon Deity "Mahamayuri" 44  Normal
    , Demon Enigma "Kangiten" 62  Normal
    , Demon Enigma "Kinmamon" 42  Normal
    , Demon Enigma "Futotama" 23  Normal
    , Demon Enigma "Billiken" 15  Normal
    , Demon Entity "Alilat" 82  Normal
    , Demon Entity "Hachiman" 76  Normal
    , Demon Entity "Albion" 70  Normal
    , Demon Entity "Ometeotl" 61  Normal
    , Demon Entity "Black Maria" 55  Normal
    , Demon Zealot "Dionysus" 74  Normal
    , Demon Zealot "Attis" 66  Normal
    , Demon Zealot "Aramisaki" 52  Normal
    , Demon Zealot "Ogun" 37  Normal
    , Demon Megami "Izanami" 83  Normal
    , Demon Megami "Anat" 71  Normal
    , Demon Megami "Norn" 63  Normal
    , Demon Megami "Lakshmi" 58  Normal
    , Demon Megami "Pallas Athena" 50  Normal
    , Demon Megami "Tlazolteotl" 43  Normal
    , Demon Megami "Scathach" 36  Normal
    , Demon Megami "Fortuna" 30  Normal
    , Demon Fury "Shiva" 85  Normal
    , Demon Fury "Susano-o" 72  Normal
    , Demon Fury "Zaou Gongen" 64  Normal
    , Demon Fury "Yama" 60  Normal
    , Demon Fury "Kartikeya" 59  Normal
    , Demon Fury "Seiten Taisei" 49  Normal
    , Demon Fury "Tonatiuh" 38  Normal
    , Demon Lady "Xi Wangmu" 81  Normal
    , Demon Lady "Kali" 75  Normal
    , Demon Lady "Cybele" 61  Normal
    , Demon Lady "Diana" 52  Normal
    , Demon Lady "Hariti" 45  Normal
    , Demon Lady "Kushinada" 41  Normal
    , Demon Lady "Dzelarhons" 36  Normal
    , Demon Lady "Pele" 29  Normal
    , Demon Kishin "Thor N" 72  Normal
    , Demon Kishin "Marici" 71  Normal
    , Demon Kishin "Bishamonten" 66  Normal
    , Demon Kishin "Thor" 58  Normal
    , Demon Kishin "Futsunushi" 53  Normal
    , Demon Kishin "Zhong Kui" 47  Normal
    , Demon Kishin "Tyr" 41  Normal
    , Demon Kishin "Kotoshironushi" 33  Normal
    , Demon Kishin "Hitokotonushi" 24  Normal
    , Demon Vile "Mada" 77  Dark
    , Demon Vile "Pales" 74  Dark
    , Demon Vile "Saturnus" 65  Dark
    , Demon Vile "Tezcatlipoca" 58  Dark
    , Demon Vile "Kanaloa" 49  Dark
    , Demon Vile "Pachacamac" 41  Dark
    , Demon Vile "Mishaguji" 35  Dark
    , Demon Reaper "Mot" 72  Dark
    , Demon Reaper "Cernunnos" 63  Dark
    , Demon Reaper "Guedhe" 54  Dark
    , Demon Reaper "DB Busters" 49  Dark
    , Demon Reaper "Chernobog N" 48  Dark
    , Demon Reaper "Chernobog" 47  Dark
    , Demon Reaper "Persephone" 38  Dark
    , Demon Genma "Heimdall" 66  Normal
    , Demon Genma "Hanuman" 60  Normal
    , Demon Genma "Tlaloc" 53  Normal
    , Demon Genma "Kresnik" 46  Normal
    , Demon Genma "Cu Chulainn" 39  Normal
    , Demon Genma "Nata Taishi" 30  Normal
    , Demon Genma "Ba Da Wang" 23  Normal
    , Demon Genma "Bes" 17  Normal
    , Demon Yoma "Valkyrie" 60  Normal
    , Demon Yoma "Peri" 53  Normal
    , Demon Yoma "Roitschaggata" 44  Normal
    , Demon Yoma "Rolwoy" 39  Normal
    , Demon Yoma "Shiwanna" 32  Normal
    , Demon Yoma "Maruts" 25  Normal
    , Demon Yoma "Apsaras" 16  Normal
    , Demon Yoma "Vodyanik" 9   Normal
    , Demon Fairy "Oberon" 58  Normal
    , Demon Fairy "Titania N" 53  Normal
    , Demon Fairy "Titania" 52  Normal
    , Demon Fairy "Troll" 47  Normal
    , Demon Fairy "Setanta" 43  Normal
    , Demon Fairy "Vivian" 40  Normal
    , Demon Fairy "Leprechaun" 33  Normal
    , Demon Fairy "Silky" 24  Normal
    , Demon Fairy "Pyro Jack" 20  Normal
    , Demon Fairy "Jack Frost" 15  Normal
    , Demon Fairy "Goblin" 10  Normal
    , Demon Fairy "Penanggal" 7   Normal
    , Demon Fairy "Pixie N" 3   Normal
    , Demon Fairy "Pixie" 2   Normal
    , Demon Night "Nyx" 56  Normal
    , Demon Night "Kaiwan" 50  Normal
    , Demon Night "Succubus" 49  Normal
    , Demon Night "Incubus" 42  Normal
    , Demon Night "Black Frost" 34  Normal
    , Demon Night "Wild Hunt" 33  Normal
    , Demon Night "Sand Man" 21  Normal
    , Demon Night "Kikimora" 14  Normal
    , Demon Night "Mokoi" 7   Normal
    , Demon Tyrant "Mitra" 80  Dark
    , Demon Tyrant "Nergal" 73  Dark
    , Demon Tyrant "Surt" 65  Dark
    , Demon Tyrant "Loki" 59  Dark
    , Demon Tyrant "Loki N" 54  Dark
    , Demon Tyrant "Tzitzimitl" 53  Dark
    , Demon Tyrant "Balor" 48  Dark
    , Demon Tyrant "Moloch" 37  Dark
    , Demon Herald "Lucifrost" 81  Normal
    , Demon Herald "Metatron" 80  Normal
    , Demon Herald "Sraosha" 73  Normal
    , Demon Herald "Sandalphon" 62  Normal
    , Demon Herald "Armaiti" 55  Normal
    , Demon Herald "Azrael" 48  Normal
    , Demon Herald "Israfel" 42  Normal
    , Demon Herald "Melchizedek" 35  Normal
    , Demon Divine "Cherub N" 65  Normal
    , Demon Divine "Cherub" 64  Normal
    , Demon Divine "Throne" 57  Normal
    , Demon Divine "Dominion" 51  Normal
    , Demon Divine "Virtue" 44  Normal
    , Demon Divine "Power" 36  Normal
    , Demon Divine "Principality" 29  Normal
    , Demon Divine "Archangel" 21  Normal
    , Demon Divine "Angel" 14  Normal
    , Demon Fallen "Murmur" 63  Normal
    , Demon Fallen "Paimon" 52  Normal
    , Demon Fallen "Botis" 46  Normal
    , Demon Fallen "Halphas" 40  Normal
    , Demon Fallen "Bifrons" 34  Normal
    , Demon Fallen "Forneus" 30  Normal
    , Demon Fallen "Nisroc" 24  Normal
    , Demon Fallen "Shax" 17  Normal
    , Demon Fallen "Seere" 10  Normal
    , Demon Dragon "Vritra" 70  Normal
    , Demon Dragon "Gucumatz" 62  Normal
    , Demon Dragon "Illuyankas" 56  Normal
    , Demon Dragon "Seiryu" 46  Normal
    , Demon Dragon "Makara" 38  Normal
    , Demon Snake "Ouroboros" 64  Normal
    , Demon Snake "Orochi" 57  Normal
    , Demon Snake "Hoyau Kamui" 50  Normal
    , Demon Snake "Mizuchi" 43  Normal
    , Demon Snake "Vouivre" 36  Normal
    , Demon Snake "Nozuchi" 29  Normal
    , Demon Snake "Yato no Kami" 22  Normal
    , Demon Drake "Ym" 69  Dark
    , Demon Drake "Fafnir" 56  Dark
    , Demon Drake "Mushussu" 50  Dark
    , Demon Drake "Python" 39  Dark
    , Demon Drake "Tuna Roa" 31  Dark
    , Demon Drake "Bai Suzhen" 21  Dark
    , Demon Drake "Zhu Tun She" 12  Dark
    , Demon Avian "Garuda" 63  Normal
    , Demon Avian "Suzaku" 56  Normal
    , Demon Avian "Yatagarasu" 50  Normal
    , Demon Avian "Thunderbird" 42  Normal
    , Demon Avian "Phoenix" 37  Normal
    , Demon Flight "Da Peng" 55  Normal
    , Demon Flight "Rukh" 48  Normal
    , Demon Flight "Tangata Manu" 39  Normal
    , Demon Flight "Tuofei" 29  Normal
    , Demon Flight "Gu Huo Niao" 17  Normal
    , Demon Flight "Harpy" 9   Normal
    , Demon Raptor "Hresvelgr" 64  Dark
    , Demon Raptor "Anzu" 51  Dark
    , Demon Raptor "Camazotz" 36  Dark
    , Demon Raptor "Gurr" 25  Dark
    , Demon Raptor "Moh Shuuvu N" 16  Dark
    , Demon Raptor "Moh Shuuvu" 15  Dark
    , Demon Raptor "Onmoraki" 8   Dark
    , Demon Avatar "Barong" 64  Normal
    , Demon Avatar "Kaiming Shou" 59  Normal
    , Demon Avatar "Anubis" 51  Normal
    , Demon Avatar "Genbu" 43  Normal
    , Demon Avatar "Kamapua'a" 32  Normal
    , Demon Holy "Sleipnir" 55  Normal
    , Demon Holy "Qing Niuguai" 48  Normal
    , Demon Holy "Byakko" 40  Normal
    , Demon Holy "Airavata" 29  Normal
    , Demon Holy "Buraq" 22  Normal
    , Demon Holy "Shiisaa" 15  Normal
    , Demon Holy "Heqet" 9   Normal
    , Demon Beast "Cerberus N" 61  Normal
    , Demon Beast "Cerberus" 60  Normal
    , Demon Beast "Ammut" 53  Normal
    , Demon Beast "Orthrus" 45  Normal
    , Demon Beast "Kaso" 37  Normal
    , Demon Beast "Dormarth" 32  Normal
    , Demon Beast "Nekomata N" 29  Normal
    , Demon Beast "Nekomata" 28  Normal
    , Demon Beast "Katakirauwa" 22  Normal
    , Demon Beast "Cunhur" 15  Normal
    , Demon Beast "Gally Trot" 6   Normal
    , Demon UMA "Hare of Inaba" 50  Normal
    , Demon UMA "Chupacabra" 35  Normal
    , Demon UMA "Kuda" 20  Normal
    , Demon UMA "Oliver Zero" 6   Normal
    , Demon UMA "Oliver" 5   Normal
    , Demon Wilder "Fenrir" 61  Dark
    , Demon Wilder "Catoblepas" 52  Dark
    , Demon Wilder "Cabracan" 45  Dark
    , Demon Wilder "Pellaidh" 37  Dark
    , Demon Wilder "Jersey Devil" 27  Dark
    , Demon Wilder "Jueyuan" 23  Dark
    , Demon Wilder "Chagrin" 15  Dark
    , Demon Wilder "Hellhound" 7   Dark
    , Demon Jirae "Muspell" 67  Normal
    , Demon Jirae "Gogmagog" 54  Normal
    , Demon Jirae "Tlaltecuhtli" 46  Normal
    , Demon Jirae "Duergar" 38  Normal
    , Demon Jirae "Tenong Cut" 25  Normal
    , Demon Jirae "Puts" 18  Normal
    , Demon Jirae "Koropokkuru" 13  Normal
    , Demon Jirae "Knocker N" 4   Normal
    , Demon Jirae "Knocker" 3   Normal
    , Demon Brute "Yaksa N" 56  Normal
    , Demon Brute "Yaksa" 55  Normal
    , Demon Brute "Shiki-Ouji" 51  Normal
    , Demon Brute "Berserker" 49  Normal
    , Demon Brute "Nyalmot" 41  Normal
    , Demon Brute "Ikusa" 35  Normal
    , Demon Brute "Galley Beggar" 30  Normal
    , Demon Brute "Dokkaebi" 23  Normal
    , Demon Brute "Bilwis" 16  Normal
    , Demon Brute "Azumi" 8   Normal
    , Demon Femme "Rangda" 59  Normal
    , Demon Femme "Volvo" 51  Normal
    , Demon Femme "Amazon" 43  Normal
    , Demon Femme "Jahi" 34  Normal
    , Demon Femme "La Llorona" 27  Normal
    , Demon Femme "Leanan Sidhe" 19  Normal
    , Demon Femme "Acheri" 12  Normal
    , Demon Jaki "Grendel" 59  Dark
    , Demon Jaki "Girimehkala" 51  Dark
    , Demon Jaki "Rakshasa" 43  Dark
    , Demon Jaki "Gashadokuro" 34  Dark
    , Demon Jaki "Lham Dearg" 26  Dark
    , Demon Jaki "Wendigo" 19  Dark
    , Demon Jaki "Ippon-Datara" 13  Dark
    , Demon Jaki "Gremlin" 5   Dark
    , Demon Tree "Haoma" 55  Normal
    , Demon Tree "Kukunochi" 48  Normal
    , Demon Tree "Mayahuel" 40  Normal
    , Demon Tree "Narcissus" 30  Normal
    , Demon Tree "Oshira-sama" 21  Normal
    , Demon Wood "Erlkonig" 50  Dark
    , Demon Wood "Alraune" 40  Dark
    , Demon Wood "Skogsra" 30  Dark
    , Demon Wood "Shan Xiao" 20  Dark
    , Demon Wood "Jubokko" 10  Dark
    , Demon (Element Salamander) "Salamander" 28  Normal
    , Demon (Element Undine) "Undine" 24  Normal
    , Demon (Element Sylph) "Sylph N" 21  Normal
    , Demon (Element Sylph) "Sylph" 20  Normal
    , Demon (Element Gnome) "Gnome" 16  Normal
    , Demon (Mitama Saki) "Saki Mitama" 35  Normal
    , Demon (Mitama Kusi) "Kusi Mitama" 30  Normal
    , Demon (Mitama Nigi) "Nigi Mitama" 25  Normal
    , Demon (Mitama Ara) "Ara Mitama" 20  Normal
    , Demon Haunt "Vetala" 57  Dark
    , Demon Haunt "Kudlak" 46  Dark
    , Demon Haunt "Greyman" 37  Dark
    , Demon Haunt "Yaka" 29  Dark
    , Demon Haunt "Churel" 19  Dark
    , Demon Haunt "Obariyon" 11  Dark
    , Demon Haunt "Preta" 3   Dark
    , Demon Undead "Cosmo Zombie" 44  Dark
    , Demon Undead "Headless Rider" 24  Dark
    , Demon Undead "Padlock" 11  Dark
    , Demon Undead "Drag Queen" 4   Dark
    , Demon Undead "Facebind" 1   Dark
    , Demon Spirit "Wicker Man" 45  Dark
    , Demon Spirit "Dybbuk" 32  Dark
    , Demon Spirit "Garrote" 18  Dark
    , Demon Spirit "Quicksilver" 10  Dark
    , Demon Spirit "Poltergeist" 2   Dark
    , Demon Foul "Shadow" 53  Dark
    , Demon Foul "Urban Terror" 31  Dark
    , Demon Foul "Douman" 26  Dark
    , Demon Foul "Mad Gasser" 25  Dark
    , Demon Foul "Night Stalker" 13  Normal
    , Demon Foul "Slime" 1   Dark
    ]

-- NOT FUSABLE:
-- , Demon Rumor "Purple Mirror" 49 
-- , Demon Rumor "Red Cape" 42 
-- , Demon Rumor "Reiko Kashima" 13 
-- , Demon Rumor "Turbo-Granny" 9  
-- , Demon Rumor "Kamiotoko" 4  
-- , Demon Hero "Huang Di" 83 
-- , Demon Hero "Rama" 77 
-- , Demon Hero "Saladin" 68 
-- , Demon Hero "Siegfried" 61 
-- , Demon Hero "Jeanne D'Arc" 54 
-- , Demon General "Masakado" 81 
-- , Demon General "Longinus" 65 
-- , Demon General "Yoshitsune" 57 
-- , Demon General "Hagen" 51 
-- , Demon General "Lanling Wang" 44 
-- , Demon Ranger "Frost Five" 55 
-- , Demon Ranger "Milky Frost" 45 
-- , Demon Ranger "Strawberry Frost" 40 
-- , Demon Ranger "Lemon Frost" 35 
-- , Demon Ranger "Melon Frost" 30 
-- , Demon Ranger "B Hawaii Frost" 25 
