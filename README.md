# soul-haskers

## About

A CLI utility to determine the result of a demon fusion in the video game
*Shin Megami Tensei: Devil Summoner: Soul Hackers* (specifically, the 3DS port).

It currently only works for binary fusions of non-Element, non-Mitama demons,
but I may extend the functionality in the future.

The demon info was taken from [this useful
tool](https://aqiu384.github.io/megaten-fusion-tool/dssh/demons), which is a
fusion guide that's more fully-featured than this program. My goal in writing
this was to learn more about Haskell, as opposed to creating a fully-featured or
useful fusion calculator.

## TODO

### Necessary features/fixes

- update alignment information in DemonList.hs (currently the non-dark demons
  are all listed as `Normal`)
- implement Element fusions
- implement Mitama fusions

### Additional/interesting features

- provide more information in the `Demon` data structure (e.g., law -- chaos
  alignment, possibly stats)
- implementing ternary fusions
- fusion search function (given a list of demons, list all the possible fusions)
