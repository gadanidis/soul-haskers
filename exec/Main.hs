module Main where

import Fusion (binaryFuse, findDemon, showDemon)
import DemonList (allDemons)

import System.IO (hFlush, stdout)
import Data.Maybe (fromJust, isNothing)

prompt :: String -> IO String
prompt text = do
    putStr text
    hFlush stdout
    getLine

main :: IO ()
main = do
    input1 <- prompt "First demon: "
    input2 <- prompt "Second demon: "
    let demon1 = findDemon input1 allDemons
        demon2 = findDemon input2 allDemons
    if any isNothing [demon1, demon2]
       then putStrLn "invalid demon!"
       else do
           let output = binaryFuse (fromJust demon1) (fromJust demon2)
           if isNothing output
              then putStrLn "The two demons cannot be fused!"
              else do 
                  putStrLn "The result of the fusion would be:"
                  putStrLn (showDemon $ fromJust output)
